#!/bin/bash

function ProgressBar {
# Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")

# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:                           
# 1.2.1.1 Progress : [########################################] 100%
printf "\rProgress : [${_fill// /\#}${_empty// /-}] ${_progress}%%"
}

START=315000
#END=100
END=782469
INCREMENT=5000
for i in $(seq $START $INCREMENT $END);\
 do 
    sleep 0.1
    ProgressBar ${i} ${END}
    N=$[i+INCREMENT] 
    printf "\tgathering dates %s to %s" "$i" "$N"
    python3 ./bin/date_gather.py --start_num $i --end_num $N --output_folder "./data"; 
done
printf '\nFinished!\n'
