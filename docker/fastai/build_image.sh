#!/bin/bash
TAG=tonychangcsp/fastai:v100 
#DOCKERFILE=fastai.v070
DOCKERFILE=Dockerfile.fastai.v100
docker build --no-cache \
	-t $TAG \
	-f ./$DOCKERFILE .
