#!/bin/bash
FILES=$(pwd)/data/*
for f in $FILES
do 
    A=$(wc -l $f | awk '{ print $1}')
    if [ $A = 1 ]
        then
            rm $f
    fi
done
