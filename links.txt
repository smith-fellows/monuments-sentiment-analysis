# list of links for national monument sentiment

# medium has 90000 comments summarized in a single google document
https://medium.com/westwise/new-analysis-shows-national-monument-support-dominates-public-comment-period-7550888175e

# official regulations.gov list of 782468 comments from May 11 to July 10 2017

# link for the first comment begins below
https://www.regulations.gov/docketBrowser?rpp=50&so=DESC&sb=commentDueDate&po=782450&dct=PS&D=DOI-2017-0002

# final comment (July 10 2017)

