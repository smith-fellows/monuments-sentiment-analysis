# Smith Fellows Sentiment analysis of National Parks and Monuments

![bearsear_npr](https://media.npr.org/assets/img/2017/01/09/crane-petroglyph-josh-ewing-1920x1080_wide-f4117ba36b6977379cab7e1a004619232a6ac7ed.jpg?s=1400)

# Current Status
In November 2018, we completed building and applying the ULMFit Language Model from ![fast.ai](http://nlp.fast.ai/). In this language model, we use transfer learning to apply the WikiText-103 language model to our corpus of words from 100,000 public comments. The word embedding can be visualized and explored using the ![tensorboard projector](http://projector.tensorflow.org/?config=https://ai4changstorage.blob.core.windows.net/0796c2fba53343048e085e2d2c0230ee/smith-sentiment/smith_projector_config.json).

# Background
In December 2016, former President Barack Obama designated Bears Ears National Monument under federal protection status following years of advocacy from local landowners and Native American tribes. This designation sought to not only protect the area for its natural resources and amenities, but also protections based on the Antiquities Act of 1906, which estabilishes archelogical sites on public lands for their educational, scientific, and cultural values. This designation was not without contraversy and became a subject of political interest upon the new presidential administration.  

In May 2017, President Trump and Interior Secretary Ryan Zinke reviewed 21 years of national monument designations, including Bears Ears National Monument. Part of this review required a 60-day public comment period. In that time span over 700,000 (currently 781934 downloaded) public comments were made, but few analysis have been made available of the general sentiment for the monuments. Since, then President Trump and Secretary Zinke have made executive orders to reduce the size of many of the monument designations, but without addressing what the sentiment of most Americans feel.

# Objective
This project will attempt to characterize the sentiment of these ~700,000 comments and present an objective analysis of the public opinion of National Monuments from public records. Previous efforts to characterize public sentiment typically involve representative sampling of the data, but may not encompass the full scope of public opinion. We will utilize a deep learning framework to automate classification all public comment general opinions.

# Prerequisites

- [Git](https://gist.github.com/derhuerst/1b15ff4652a867391f03)
- [Docker CE](https://www.docker.com/community-edition#/download)

## Installing
Terminal installation:
```
git clone https://gitlab.com/tonychang/smith-monuments-sentiment
docker pull tonychangcsp/chromedriver:latest
```

## Deployment
From terminal (on Linux), if you are on OSX or Windows start your Docker instance first. 
```
cd smith-monuments-sentiment
./run.sh 
```
In the docker instance run the python script ```comment_gather.py``` with flags ```--start_num``` and ```--end_num``` denoting what comment to begin and end downloads on. Note that comment begin on number 2 and end at 782469.

### Example 
Download comments 2-100: 
```
python3 ./bin/comment_gather.py --start_num 2 --end_num 100 
```
Comments will be stored in ```/data/comments_<start_num>_<end_num>.csv```

## Authors
**Tony Chang** - *Initial work* - [CSP](https://gitlab.com/tonychang)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgements

* David H. Smith Conservation Fellowship

