PORT=8080
IP=0.0.0.0
DOCKERIMAGE=tonychangcsp/fastai
TAG=v100
nvidia-docker run -it \
    -p $PORT:$PORT \
    -v $(pwd):/contents \
    -v /datadrive:/data \
    -w /contents --rm \
    $DOCKERIMAGE:$TAG  \
        jupyter notebook \
            --port $PORT \
            --ip $IP \
            --no-browser --allow-root


